require "json"

class UsersController < ApplicationController
  include NotesHelper
  include UsersHelper
  before_action :set_user, only: [:show, :edit, :update, :destroy]

  # GET /users
  # GET /users.json
  def index
    @users = User.all
  end

  # GET /users/1
  # GET /users/1.json
  def show
  end

  # GET /users/new
  def new
    @newuser = User.new
    @user = User.new
  end

  # GET /users/1/edit
  def edit
  end

  # POST /users
  # POST /users.json
  def create
    puts "params = " + params.to_s
    if (params[:commit] == 'Sign In')
      signin
      return
    end

    # Create new user
    if (not user_exists(params[:user][:email]))
      @newuser = User.new(user_params)
      respond_to do |format|
        if @newuser.save
          # Cookie the newly created user
          cookies[:current_user] =  { 'id' => @newuser.id, 'name' => @newuser.name }.to_json
          notice_msg = 'User was successfully created. '
          puts "cookies[:content] = " + cookies[:content].to_s
          if (cookies[:content])
            create_note(cookies[:content], @newuser.id)
            notice_msg = notice_msg + 'Note was successfully created.'
          end
          format.html { redirect_to notes_path, notice: notice_msg }
        else
          format.html { render :new }
          format.json { render json: @newuser.errors, status: :unprocessable_entity }
        end
      end
    else
      @newuser = User.new  
      respond_to do |format|
        format.html { render :new, notice: 'User with specified email already exists.' }
      end
    end
  end

  def signin
    puts 'in signin method'
    signedin_user = signin_user(params[:user][:email], params[:user][:password])
    respond_to do |format|
      if signedin_user
        @user = signedin_user
        puts signedin_user.id

        # Cookie the newly created user
        cookies[:current_user] =  { 'id' => @user.id, 'name' => @user.name }.to_json
        notice_msg = 'Login successful. '
        if (cookies[:content])
          create_note(cookies[:content], @user.id)
          notice_msg = notice_msg + 'Note was successfully created.'
        end
        format.html { redirect_to notes_path, notice: notice_msg }
      else
        @user = User.new
        format.html { render :new, notice: 'Email/password combination not found' }
        #format.json { render json: 'Email/password combination not found', status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update
    respond_to do |format|
      if @user.update(user_params)
        format.html { redirect_to @user, notice: 'User was successfully updated.' }
        format.json { render :show, status: :ok, location: @user }
      else
        format.html { render :edit }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to users_url, notice: 'User was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(:name, :password, :email)
    end
end
