require "json"

class NotesController < ApplicationController
  include NotesHelper
  before_action :set_note, only: [:show, :edit, :update, :destroy]

  # GET /notes
  # GET /notes.json
  def index
    @notes = Note.includes(:user).order('notes.created_at DESC').all
    @note = Note.new
  end

  # GET /notes/1
  # GET /notes/1.json
  def show
  end

  # GET /notes/new
  def new
    @note = Note.new
  end

  # GET /notes/1/edit
  def edit
  end

  # POST /notes
  # POST /notes.json
  def create
    # Get the note contents
    content = params[:note][:content]

    # Check if we have a logged in user
    user = JSON.load(cookies[:current_user])

    # Cookie the note so that the user controller can create the note
    # before redirecting back to the notes page
    cookies[:content] = content

    # If not, then have user sign up
    if user.nil?
      redirect_to new_user_path, content: content
      return
    end

    # Create the note otherwise
    @note = create_note(content, user['id'])
    respond_to do |format|
      if @note
        format.html { redirect_to notes_path, notice: 'Note was successfully created.' }
      #  format.json { render :index, status: :created }
      else
        format.html { render :index }
        format.json { render json: @note.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /notes/1
  # PATCH/PUT /notes/1.json
  def update
    user = JSON.load(cookies[:current_user])
    respond_to do |format|
      if @note.update({ :content => params[:note][:content], :user_id => user['id'] })
        format.html { redirect_to notes_path, notice: 'Note was successfully updated.' }
      #  format.json { render :show, status: :ok, location: @note }
      else
        format.html { render :edit }
        format.json { render json: @note.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /notes/1
  # DELETE /notes/1.json
  def destroy
    @note.destroy
    respond_to do |format|
      format.html { redirect_to notes_url, notice: 'Note was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_note
      @note = Note.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def note_params
      params.require(:note).permit(:content, :user_id)
    end
end
