module UsersHelper
	def get_user(user_email)
		User.find_by email: user_email
	end

	def signin_user(user_email, user_password)
		user = get_user(user_email)
		if user && user.password == user_password
			return user
		end
		return nil
	end

	def user_exists(user_email)
		get_user(user_email) ? true : false
	end
end
