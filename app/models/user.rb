class User < ActiveRecord::Base
	has_many :notes

	validates :name, presence: true
	validates :password, presence: true
	validates :email, presence: true
end
